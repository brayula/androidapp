package com.example.brianagina.anew;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class SecondImageActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(R.string.Automobile);
        setContentView(R.layout.activity_second_image);
    }
    public void backHome(View view) {
        Intent backHomeActivity = new Intent(this, MainActivity.class);
        startActivity(backHomeActivity);
    }
}

package com.example.brianagina.anew;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(R.string.Main);
        setContentView(R.layout.activity_main);
    }

    public void firstImage(View view){
        Intent startFirstActivity = new Intent(this,FirstImageActivity.class);
        startActivity(startFirstActivity);
    }

    public void secondImage(View view){
        Intent startSecondActivity = new Intent(this, SecondImageActivity.class);
        startActivity(startSecondActivity);
    }

    public void thirdImage(View view){
        Intent startThirdActivity = new Intent(this, ThirdImageActivity.class);
        startActivity(startThirdActivity);
    }

    public void backHome(View view){
        Intent backHomeActivity = new Intent(this, MainActivity.class);
        startActivity(backHomeActivity);
    }
}
